; Name:         basic.asm
; Author:       e2dk4r
; Version:      v1.0
; Creation:     2020-02-22
; Updated:      2020-02-22 (e2dk4r)
; Description:  A template file to easily create amd64 assembly files

;===============================================================================
; section containing un-initialized data
SECTION .bss

;===============================================================================
; section containing initialized data
SECTION .data


;===============================================================================
; section containing code
SECTION .text
GLOBAL _start

;*******************************************************************************
; Foo:          a short description
; VERSION:      v1.0
; UPDATED:      2020-02-22
; INPUT:        <none>
; RETURN:       <none>
; MODIFIES:     <none> 
; CALLS:        <none>
; DESCRIPTION:  a long description 
;               ...
; NOTE:         additional notes  
Foo:
    ret         ; return to caller

;===============================================================================
; MAIN PROGRAM
_start:
    jmp     Foo         ; demonstrate function call

    ; exit program successfully
    mov     rdi, 0      ; status code 0: meaning ok
    mov     rax, 60     ; kernel sys_exit (60) code
    syscall             ; make kernel call
