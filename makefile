#-------------------------------------------------------------------------------
# MAKE CONFIG
#-------------------------------------------------------------------------------
TZ 		  := UTC
LC_ALL    := C.UTF-8
SHELL     := bash
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
.RECIPEPREFIX = >

#-------------------------------------------------------------------------------
# VARIABLES
#-------------------------------------------------------------------------------
MKDIR = mkdir -p
LN = ln -sf

#-------------------------------------------------------------------------------
# TARGETS
#-------------------------------------------------------------------------------
all: rcsh asoundrc xinitrc aria2 git neovim mpv zathura

XDG_CONFIG_DIR:
> $(MKDIR) $(HOME)/.config

XDG_USER_DIRS: XDG_CONFIG_DIR
> $(LN) $(CURDIR)/xdg/user-dirs.dirs $(HOME)/.config/user-dirs.dirs

#-------------------------------------------------------------------------------
## SHELLS
#-------------------------------------------------------------------------------
rcsh: XDG_CONFIG_DIR
> $(MKDIR) $(HOME)/.config/rcsh
> $(LN) $(CURDIR)/rcsh/env  $(HOME)/.config/rcsh/env
> $(LN) $(CURDIR)/rcsh/func  $(HOME)/.config/rcsh/func
> $(LN) $(CURDIR)/rcsh/prompt  $(HOME)/.config/rcsh/prompt
> $(LN) $(CURDIR)/rcsh/rcrc  $(HOME)/.config/rcsh/rcrc

bash:
> $(LN) $(CURDIR)/bash/bashrc $(HOME)/bashrc
> $(LN) $(CURDIR)/bash/bash_profile $(HOME)/bash_profile
> $(LN) $(CURDIR)/bash $(HOME)/.config/bash

fish: XDG_CONFIG_DIR
> $(LN) $(CURDIR)/fish $(HOME)/.config/fish

#-------------------------------------------------------------------------------
## AUDIO
#-------------------------------------------------------------------------------
asoundrc:
> $(LN) $(CURDIR)/alsa/asoundrc $(HOME)/.asoundrc

#-------------------------------------------------------------------------------
## DISPLAY SERVER
#-------------------------------------------------------------------------------
xinitrc:
> $(LN) $(CURDIR)/xinit/xinitrc $(HOME)/.xinitrc

#-------------------------------------------------------------------------------
## APPS
#-------------------------------------------------------------------------------
aria2: XDG_CONFIG_DIR
> $(MKDIR) $(HOME)/.config/aria2
> $(LN) $(CURDIR)/aria2/aria2.conf $(HOME)/.config/aria2/aria2.conf

git: XDG_CONFIG_DIR
> $(LN) $(CURDIR)/git/config $(HOME)/gitconfig

neovim: XDG_CONFIG_DIR
> $(LN) $(CURDIR)/nvim $(HOME)/.config/nvim

mpv: XDG_CONFIG_DIR
> $(LN) $(CURDIR)/mpv $(HOME)/.config/mpv

alacritty: XDG_CONFIG_DIR
> $(LN) $(CURDIR)/alacritty $(HOME)/.config/alacritty

zathura: XDG_CONFIG_DIR
> $(LN) $(CURDIR)/zathura $(HOME)/.config/zathura

.PHONY: all asoundrc xinitrc aria2 bash fish git neovim mpv alacritty
