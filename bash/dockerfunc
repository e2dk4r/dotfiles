#!/bin/bash -eu -o pipefail -c

ensure_dir() {
    if [[ ! -d $1 ]]; then
        mkdir -p $1
    fi
}

firefox() {
    local cache_dir="${HOME}/.cache/mozilla"
    local config_dir="${HOME}/.config/mozilla"
    local download_dir="${HOME}/download"

    ensure_dir $cache_dir
	ensure_dir $config_dir
	ensure_dir $download_dir

	docker run --rm --detach \
	  --net host \
	  --name "firefox" \
	  --shm-size 2g \
	  --device /dev/dri \
	  --device /dev/snd \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",destination="/tmp/.X11-unix",ro=true \
	  --mount type=bind,source="${cache_dir}",destination="/home/e2dk4r/.cache/mozilla" \
	  --mount type=bind,source="${config_dir}",destination="/home/e2dk4r/.mozilla" \
	  --mount type=bind,source="${download_dir}",destination="/home/e2dk4r/Downloads" \
	  e2dk4r/firefox "$@"
}

torbrowser() {
	local profile_root="${HOME}/.config/torbrowser"
	local profile_local="${HOME}/.cache/torbrowser"
	local download_dir="${HOME}/download"

	ensure_dir $profile_root
	ensure_dir $profile_local
	ensure_dir $download_dir

	podman run --rm \
	  --name "torbrowser" \
	  --device /dev/dri \
	  --env DISPLAY=$DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",target="/tmp/.X11-unix",ro \
	  --mount type=bind,source="${profile_root}",destination="/app/Browser/TorBrowser/Data/Browser/profile.default" \
	  --mount type=bind,source="${profile_local}",destination="/app/Browser/TorBrowser/Data/Browser/Caches/profile.default" \
	  --mount type=bind,source="${download_dir}",destination="/app/Browser/Downloads" \
	  --shm-size 2g \
	  e2dk4r/torbrowser:latest
}

qr() {
    podman run --rm mpercival/qrterminal:latest "$@"
}

zathura() {
	local config_dir="${HOME}/.config/zathura"
	local fullpath=$(realpath $1)
	local dirname=$(dirname $fullpath)
	local filename=$(basename $fullpath)

	ensure_dir $config_dir
	if [[ ! -e $1 ]]; then
	    echo "file not exists"
	    return 1
    fi

	docker run --rm --detach \
	  --net=none \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",destination="/tmp/.X11-unix",readonly \
	  --mount type=bind,source="${config_dir}",destination="/home/e2dk4r/.config/zathura" \
	  --mount type=bind,source="${dirname}",destination="/home/e2dk4r/files",readonly \
	  --workdir /home/e2dk4r/files \
	  e2dk4r/zathura \
	    --config-dir="/home/e2dk4r/.config/zathura" \
	    --data-dir="/home/e2dk4r/.config/zathura" \
	    $filename
}

deezer() {
    local config_dir="$HOME/.config/deezloader"
    local download_dir="$HOME/music"

	ensure_dir $config_dir
	ensure_dir $download_dir

	podman run --rm --detach \
	  --name deezloader \
	  -p 1730:1730 \
	  --env PUID=1000 \
	  --env PGID=1000 \
	  --mount type=bind,source="$config_dir",destination="/config" \
	  --mount type=bind,source="$download_dir",destination="/downloads" \
	  bocki/deezloaderrmx
}

office() {
	local download_dir="$HOME/download"

	docker run --rm --detach \
	  --net=none \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",destination="/tmp/.X11-unix",readonly \
	  --mount type=bind,source="${download_dir}",destination="/home/e2dk4r/Downloads" \
	  --workdir /home/e2dk4r/Downloads \
	  e2dk4r/libreoffice
}

gimp() {
	local pics_dir="$HOME/pics"

	docker run --rm --detach \
	  --net=none \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",destination="/tmp/.X11-unix",readonly \
	  --mount type=bind,source="$pics_dir",destination="/home/e2dk4r/Pictures" \
	  --workdir /home/e2dk4r/Pictures \
	  e2dk4r/gimp
}
