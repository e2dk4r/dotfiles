let mapleader = ";"
call plug#begin('~/.vim/plugged')

Plug 'mileszs/ack.vim'
Plug 'cloudhead/neovim-fuzzy'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-dadbod'

Plug 'gentoo/gentoo-syntax'

call plug#end()

" ack.vim
let g:ackprg = 'ag --nogroup --nocolor --column'

" Color scheme
set background=dark

" Clipboard
set clipboard=unnamedplus

" Indentation
set tabstop=4           " Number of visual spaces per tab
set softtabstop=4       " Number of spaces in tab when editing
set shiftwidth=4        " Number of spaces to use for autoindent
"set expandtab           " Tabs are spaces
set noexpandtab
set copyindent          " Copy indentation from previous line
set autoindent          " Copy indent from current line when starting a new line

" Search
set incsearch           " Search as characters are typed
set hlsearch            " Highlight matches
set ignorecase          " Ignore case when searching
set smartcase           " Ignore case when only lower case is typed

" Other
set title titlestring=%<%F titlelen=70
set mouse=
set number              " Line numbers
set nowrap              " Do not wrap long lines
set showmatch           " Highlight matching brackets
set noswapfile          " Do not create swap file
set nolist listchars=tab:>-,eol:@,trail:.,extends:>,precedes:<
"set relativenumber      " Display line number relative to the current
"set cursorline          " Highlight the current line
"set colorcolumn=80      " Draw a vertical bar at column

" StatusLine
set statusline=
set statusline+=%#PmenuSel#
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ 

" Mappings
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

nnoremap <leader>o :FuzzyOpen<CR>
nnoremap <leader>g :FuzzyGrep<CR>

" Auto 
augroup langc
	autocmd BufNewFile,BufRead *.h,*.c set path=.,include,/usr/local/include,/usr/include
	autocmd BufNewFile,BufRead *.h,*.c set complete=.,i,t
	autocmd BufNewFile,BufRead *.h,*.c set tags+=~/.config/nvim/systags
augroup END

augroup makefile
	autocmd BufNewFile,BufRead Makefile,makefile,GNUMakefile set tw=4 sw=4 noet
augroup END

augroup yaml
	autocmd BufNewFile,BufRead *.yaml,*.yml set tw=2 sw=2 et
augroup END
