# for X11 connections to localhost
xhost +local:$USER

fn qr {
	podman run --rm mpercival/qrterminal:latest $*
}

fn deemix {
    podman run --rm --detach \
      --name deemix \
      --mount ('type=bind,source=' ^ ($HOME ^ '/.config/deemix' ) ^ ',destination=/root/.config/deemix') \
      --mount ('type=bind,source=' ^ ($HOME ^ '/music' )          ^ ',destination=/data') \
      -p 6595:6595 \
      e2dk4r/deemix
}

fn torbrowser {
    podman run --rm --detach \
      --name torbrowser \
      --shm-size 2g \
      --device /dev/dri \
      --env ('DISPLAY=' ^ $DISPLAY) \
      --mount 'type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix,ro' \
      --mount ('type=bind,source=' ^ ($HOME ^ '/.config/torbrowser' ) ^ ',destination=/app/Browser/TorBrowser/Data/Browser/profile.default') \
      --mount ('type=bind,source=' ^ ($HOME ^ '/.cache/torbrowser' )  ^ ',destination=/app/Browser/TorBrowser/Data/Browser/Caches/profile.default') \
      e2dk4r/torbrowser
}

fn gimp {
    podman run --rm --detach \
      --name gimp \
      --device /dev/dri \
      --env ('DISPLAY=' ^ $DISPLAY) \
      --mount 'type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix,ro' \
      --mount ('type=bind,source=' ^ ($HOME ^ '/pics' ) ^ ',destination=/pictures') \
      --workdir /pictures \
      e2dk4r/gimp
}

fn inkscape {
    podman run --rm --detach \
      --name inkscape \
      --device /dev/dri \
      --env ('DISPLAY=' ^ $DISPLAY) \
      --mount 'type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix,ro' \
      --mount ('type=bind,source=' ^ ($HOME ^ '/.config/inkscape' ) ^ ',destination=/root/.config/inkscape') \
      --mount ('type=bind,source=' ^ ($HOME ^ '/pics' ) ^ ',destination=/pictures') \
      --workdir /pictures \
      e2dk4r/inkscape
}

fn keepassxc {
    podman run --rm --detach \
      '--net=none' \
      --name keepassxc \
      --device /dev/dri \
      --env ('DISPLAY=' ^ $DISPLAY) \
      --mount 'type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix,ro' \
      --mount ('type=bind,source=' ^ ($HOME ^ '/.config/keepassxc' ) ^ ',destination=/root/.config/keepassxc') \
      --mount ('type=bind,source=' ^ ($HOME ^ '/.config/keepass' ) ^ ',destination=/data') \
      e2dk4r/keepassxc
}

fn libreoffice {
    podman run --rm --detach \
      '--net=none' \
      --name libreoffice \
      --env ('DISPLAY=' ^ $DISPLAY) \
      --mount 'type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix,ro' \
      --mount ('type=bind,source=' ^ ($HOME ^ '/.config/libreoffice' ) ^ ',destination=/root/.config/libreoffice') \
      --mount ('type=bind,source=' ^ ($HOME ^ '/document' ) ^ ',destination=/data') \
      --workdir /data \
      e2dk4r/libreoffice
}
