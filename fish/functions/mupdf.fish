function usage
	echo mupdf [filename]
end

function mupdf
	if test -z $argv
		usage
		return 1
	end

	if test ! -e $argv
		echo File not exists
		return 2
	end

	docker run --rm  \
	  --net=none \
	  --env DISPLAY \
	  --volume /tmp/.X11-unix:/tmp/.X11-unix:ro \
	  --device /dev/dri \
	  --volume $PWD:/home/e2dk4r/files:ro \
	  --workdir /home/e2dk4r/files \
	  e2dk4r/mupdf $argv
end
