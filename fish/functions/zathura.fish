function zathura
	set config_dir "$HOME/.config/zathura"
	set fullpath (realpath $argv[1])
	set dirname (dirname $fullpath)
	set filename (basename $fullpath)

	ensure_dir $config_dir

	# exit if file not exists
	if test ! -e $argv[1]
		return 1
	end

	docker run --rm --detach \
	  --net=none \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",destination="/tmp/.X11-unix",readonly \
	  --mount type=bind,source="$config_dir",destination="/home/e2dk4r/.config/zathura" \
	  --mount type=bind,source="$dirname",destination="/home/e2dk4r/files",readonly \
	  --workdir /home/e2dk4r/files \
	  e2dk4r/zathura --config-dir="$config_dir" --data-dir="$config_dir" $filename
end

function ensure_dir
	if test ! -d $argv[1]
		mkdir -p $argv[1]
	end
end
