function firefox
	set cache_dir  $HOME/.cache/mozilla
	set config_dir $HOME/.config/mozilla
	set download_dir "$HOME/download"

	ensure_dir $cache_dir
	ensure_dir $config_dir
	ensure_dir $download_dir

	# wayland
	#--env XDG_RUNTIME_DIR=/tmp \
	#--env WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
	#--env GDK_BACKEND=wayland \
	#--env MOZ_ENABLE_WAYLAND=1 \
	#--volume $XDG_RUNTIME_DIR/$WAYLAND_DISPLAY:/tmp/$WAYLAND_DISPLAY \

	docker run --rm --detach \
	  --net host \
	  --name "firefox" \
	  --shm-size 2g \
	  --device /dev/dri \
	  --device /dev/snd \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",destination="/tmp/.X11-unix",readonly \
	  --mount type=bind,source="$cache_dir",destination="/home/e2dk4r/.cache/mozilla" \
	  --mount type=bind,source="$config_dir",destination="/home/e2dk4r/.mozilla" \
	  --mount type=bind,source="$download_dir",destination="/home/e2dk4r/Downloads" \
	  e2dk4r/firefox $argv
end

function ensure_dir
	if test ! -d $argv[1]
		mkdir -p $argv[1]
	end
end
