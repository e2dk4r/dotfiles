function qutebrowser
	#set cache_dir  $HOME/.cache/mozilla
	#set config_dir $HOME/.config/mozilla

	#ensure_dir $cache_dir
	#ensure_dir $config_dir

	docker run --rm --detach \
	  --net host \
	  --name "qutebrowser" \
	  --mount type=bind,source="$HOME/download",target="/home/e2dk4r/Downloads" \
	  --device /dev/dri \
	  --device /dev/snd \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",target="/tmp/.X11-unix",readonly \
	  --shm-size 2g \
	  e2dk4r/qutebrowser $argv
end

function ensure_dir
	if test ! -d $argv[1]
		mkdir -p $argv[1]
	end
end
