function libreoffice
	docker run --rm --detach \
	  --net=none \
	  --env DISPLAY \
	  --volume /tmp/.X11-unix:/tmp/.X11-unix:ro \
	  --volume $HOME/document:/home/e2dk4r/Documents \
	  e2dk4r/libreoffice
end
