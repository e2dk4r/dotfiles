function torbrowser
	set profile_root "$HOME/.config/torbrowser"
	set profile_local "$HOME/.cache/torbrowser"
	set download_dir "$HOME/download"

	ensure_dir $profile_root
	ensure_dir $profile_local
	ensure_dir $download_dir

	docker run --rm --detach \
	  --name "torbrowser" \
	  --device /dev/dri \
	  --env DISPLAY \
	  --mount type=bind,source="/tmp/.X11-unix",target="/tmp/.X11-unix",readonly \
	  --mount type=bind,source=$profile_root,destination="/usr/local/bin/Browser/TorBrowser/Data/Browser/profile.default" \
	  --mount type=bind,source=$profile_local,destination="/usr/local/bin/Browser/TorBrowser/Data/Browser/Caches/profile.default" \
	  --mount type=bind,source="$download_dir",destination="/usr/local/bin/Browser/Downloads" \
	  --shm-size 2g \
	  jess/tor-browser:alpha
end

function ensure_dir
	if test ! -d $argv[1]
		mkdir -p $argv[1]
	end
end
