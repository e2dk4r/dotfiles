function gimp
	docker run --rm --detach \
	  --net=none \
	  --env DISPLAY \
	  --volume /tmp/.X11-unix:/tmp/.X11-unix:ro \
	  --volume $HOME/pics:/home/e2dk4r/Pictures \
	  --workdir /home/e2dk4r/Pictures \
	  e2dk4r/gimp
end
