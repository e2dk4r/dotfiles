function weechat
	set config_dir "$HOME/.config/weechat"
	set download_dir "$HOME/download"

	ensure_dir $config_dir
	ensure_dir $download_dir

	docker run --rm \
	  --interactive --tty \
	  --name "weechat" \
	  --mount type=bind,source="$config_dir",destination="/home/e2dk4r/.weechat" \
	  --mount type=bind,source="$download_dir",destination="/home/e2dk4r/Downloads" \
	  e2dk4r/weechat
end

function ensure_dir
	if test ! -d $argv[1]
		mkdir -p $argv[1]
	end
end
